const fs = require('fs');
const axios = require('axios');

const DEFAULT_APIURL = 'https://welldesign.oliasoft.com/api';

// Please note that if error model information is missing, or name
// points to a non-existing error model, a default MWDr4 model will
// get added by default. This is to simplify getting started with
// these fairly complex APIs.

// Changes a "unique" id that changes every hour, to make it
// easy to simulate both inserts and updates:
const fake_id = (new Date()).toISOString().substr(0, 13);

const BODY = {
  "name": 'Sepfac Create Test ' + fake_id,
  "country": "NO",
  "field": 'DELTA API TEST',
  "site": "SITE API TEST",
  "well": "DELTA WELL API TEST",
  "wellbore": "OH API TEST",
  "ext_system_name": "APIClient",
  "ext_system_id": fake_id,
  "dataset": {
    "errmod_sepfac": {
      "wells": [
        {
          "name": "DELTA 00",
          "well_id": "my_ref",
          "coordinates": {
            "datum": "wgs84",
            "loc": "UTM 39 N 596945.749 6642871.404"
          },
          "points": [
            ["m", "deg", "deg"],
            [0, 0, 0],
            [500, 0, 45],
            [1000, 60, 115],
            [1500, 80, 165],
            [2000, 80, 175],
            [3000, 85, 155]
          ],
          "holetable": [
            ["m", "m", "m"],
            [0, 10000, 0.4572]],
        },
        {
          "name": "DELTA Offset 01",
          "coordinates": {
            "datum": "wgs84",
            "loc": "UTM 39 N 596945.749 6642971.404"
          },
          "points": [
            ["m", "deg", "deg"],
            [0, 0, 0],
            [400, 10, 50],
            [1000, 40, 90],
            [1500, 60, 110],
            [3000, 60, 110]
          ],
          "holetable": [
            ["m", "m", "in"],
            [0, 10000, 12]
          ],
        },
        {
          "name": "DELTA Offset 02",
          "coordinates": {
            "datum": "wgs84",
            "loc": "UTM 39 N 597145.749 6642871.404"
          },
          "points": [
            ["m", "deg", "deg"],
            [0, 0, 0],
            [600, 30, 160],
            [1200, 50, 150],
            [1700, 60, 150],
            [4200, 60, 150]
          ],
        },
        {
          "name": "Kickoff Well",
          "id": "DELTA_kickoff",
          "kickoffix": 0,
          "kickoffmd": "700|m",
          "coordinates": {
            "datum": "wgs84",
            "loc": "UTM 39 N 597145.749 6642871.404"
          },
          "points": [
            ["m", "deg", "deg"],
            [700, -30, 60],
            [1000, -50, -50]
          ],
        }
      ],
      "targets": [
        [-300, 300, "3280.84|ft", "Entry 1"],
        [-500, "700|m", 1100, "Target 1"]
      ],
    }
  }
};

process.on('unhandledRejection', error => {
  // Will print "unhandledRejection err is not defined"
  console.log('unhandledRejection', error.message, error?.response?.data);
});

function errorExit(...args) {
  console.error(...args);
  process.exit(1);
}

async function main() {
  let {APIKEY, APIURL} = process.env;
  if (!APIKEY) errorExit('missing APIKEY');
  if (!APIURL) {
    APIURL = DEFAULT_APIURL;
    console.log('No APIURL found, using default', APIURL);
  }
  // const url = APIURL + '/createds';
  const url = APIURL + '/upsertObjectsExt';
  console.log('posting:', {url, ext_system_name: BODY.ext_system_name, ext_system_id: BODY.ext_system_id});
  const response = await axios.post(url,
    BODY, {
      withCredentials: true,
      headers: {
        Authorization: 'ApiKey ' + APIKEY,
      }
  });

  const {data} = response;

  console.log('returned', data);
}

main();
