# Api Clients

This is a repo demonstrating using Oliasoft WellDesign (OWD) APIs from
client software.

After cloning the repository, the node.js test client can be found in
the `nodejs` directory. Change into that directory and execute `npm
install` to install dependencies (only axios, for more recent node.js
installations the native `fetch` method can be used instead).

After installing dependencies the client can be tested as follows (you
need a valid APIKEY for testing):

```
APIKEY=yourapikey APIURL=yourapiurl node errmod_sepfac.js
```

This will execute the API call using the example data contained in the
`errmod_sepfac.js` script (one reference well, two offset wells). It
will report back how many offset wells were used in the calculation
and will write two charts based on the return value from the API
call. The charts are returned inside the API call using "data
urls". The `errmod_sepfac.js` demonstrates how these can be extracted
and written to files.

It will write two `.png` images, namely `minDistPlot.png` and
`minSFPlot.png`, which are the charts for minimum distances and
minimum separation factors.

If you do not give any `APIURL`, our default production URL is used
(`https://welldesign.oliasoft.com/api`). If you want to call any
privately hosted Oliasoft WellDesign instances (on Red Hat or
similar), make sure `APIURL` gets set to the proper URL.

```
APIKEY=yourapikey APIURL=yourapiurl node errmod_sepfac.js
```

Assuming everything runs correctly, images like the ones below will
get generated and returned:

![Minimum Distance Plot](img/minDistPlot.png)

![Minimum Separation Factor Plot](img/minSFPlot.png)
